import $ from 'jquery'
window.jQuery = $;
window.$ = $;


import select2 from 'select2/dist/js/select2';
import Swiper from 'swiper/swiper-bundle.esm.browser.min'
/*
ON LOAD PAGE FUNCTION
*/

jQuery( window ).on( 'load', function() {



} );

/*
INITIALIZATION FUNCTIONS
*/

jQuery( document ).ready( function( $ ) {

    $('select').select2({
        minimumResultsForSearch: 20
    });

    new Swiper('.before-after--slider', {

        pagination: {
            el:'.before-after-pagination',
        },
        navigation: {
            nextEl: '.before-after-next',
            prevEl: '.before-after-prev',
        },
        breakpoints: {
            // when window width is >= 320px
            320: {
                slidesPerView: 1,
                spaceBetween: 0
            },
            480: {
                slidesPerView: 1.5,
                spaceBetween:10
            },
            // when window width is >= 480px
            768: {
                slidesPerView: 3,
                spaceBetween: 15,
                centeredSlides: false,
            },
            1024: {
                slidesPerView: 3.5,
                spaceBetween: 30,
                centeredSlides: true,
            }
        }

    });


    var galleryThumbs = new Swiper('.services-bottom--slider', {
        loop:true,
        slidesPerView: 1,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        navigation: {
            nextEl: '.services-top--slider-prev',
            prevEl: '.services-top--slider-next',
        },

    });
    var galleryTop = new Swiper('.services-top--slider', {
        slidesPerView: 4,
        spaceBetween: 30,
        loop:true,
        navigation: {
            nextEl: '.services-top--slider-prev',
            prevEl: '.services-top--slider-next',
        },
        thumbs: {
            swiper: galleryThumbs
        },
        breakpoints: {
            320: {
                slidesPerView: 2.5,
                spaceBetween: 10
            },
            991: {
                slidesPerView: 4,
                spaceBetween: 30,
            }
        }
    });
      new Swiper('.news-section--slider', {
            slidesPerView: 4,
            spaceBetween: 30,
          breakpoints: {
              // when window width is >= 320px
              320: {
                  slidesPerView: 1,
                  spaceBetween: 0
              },
              480: {
                  slidesPerView: 1.5,
                  spaceBetween: 30
              },
              768: {
                  slidesPerView: 2.5,
                  spaceBetween: 30
              },
              // when window width is >= 640px
              1024: {
                  slidesPerView: 4,
                  spaceBetween: 30,
              }
          }
        });
      new Swiper('.video-slider', {
            slidesPerView: 3,
            spaceBetween: 30,
          loop:true,
          navigation: {
              nextEl: '.video-slider-prev',
              prevEl: '.video-slider-next',
          },
          pagination: {
              el:'.video-sliderr-pagination',
          },
        });


    var leftSlider = new Swiper('.left-slider', {
        slidesPerView: 3,
        loop:true,
        direction: 'vertical',
        spaceBetween: 30,
        centeredSlides: true,
        // mousewheel: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        navigation: {
            nextEl: '.left-slider-next',
            prevEl: '.left-slider-prev',
        },
        pagination: {
            el:'.left-slider-pagination',
        },
        breakpoints: {            // when window width i >= 320px

            320: {
                slidesPerView: 1,
            },
            480: {
                direction: 'horizontal',
                spaceBetween: 10,
                slidesPerView: 1.5,
            },
            // when window width is >= 640px
            991: {
                direction: 'vertical',
            }
        }
    });
    new Swiper('.right-slider', {
        slidesPerView: 1,

        loop:true,
        navigation: {
            nextEl: '.left-slider-next',
            prevEl: '.left-slider-prev',
        },
        pagination: {
            el:'.left-slider-pagination',
        },
        thumbs: {
            swiper: leftSlider
        }
    });
    new Swiper('.what-say--slider', {
        slidesPerView: 2,
        spaceBetween: 30,
        loop:true,
        navigation: {
            nextEl: '.what-say--slider-next',
            prevEl: '.what-say--slider-prev',
        },
        pagination: {
            el:'.what-say--slider-pagination',
        },
        breakpoints: {
            // when window width is >= 320px
            320: {
                slidesPerView: 1,
                spaceBetween: 0
            },
            // when window width is >= 480px
            480: {
                slidesPerView: 1.5,
                spaceBetween: 30
            },
            // when window width is >= 640px
            1024: {
                slidesPerView: 2,
                spaceBetween: 30,
            }
        }
    });

    new Swiper('.video-services--slider', {
        slidesPerView: 2.4,
        spaceBetween: 32,
        loop:true,
        centeredSlides: true,
        navigation: {
            nextEl: '.video-services-next',
            prevEl: '.video-services-prev',
        },
        pagination: {
            el:'.video-services-pagination',
        }
    });

    $('header .burger').on('click', function (){
        $(this)
            .toggleClass('is-active')
        $('.top-menu')
            .toggleClass('is-active')
        $('body')
            .toggleClass('no-scroll')
    })

    $('.constructor-services--content ul li span').on('click', function (){
        $(this)
            .toggleClass('is-active')
            .find('ul')
            .toggleClass('is-active')
    })
    $('.visible-list span').on('click', function (){
        $(this)
            .toggleClass('is-active')
            .closest('.price--section')
            .find('.table-content')
            .toggleClass('is-active')

    })
    $('.table-row > div:first-child').on('click', function (){
       $(this)
           .toggleClass('is-active')
           .closest('.price--section')
           .find('.table-content')
           .toggleClass('is-active')
   })
    $('.visible-specialists--list').on('click', function (){
        $(this).closest('.specialists-pages--section').find('ul').toggleClass('is-active')
    })
    $('.visible-equipment--list').on('click', function (){
        $(this).closest('.equipment-content').find('ul').toggleClass('is-active')
    })

    $('.step-one .button-box span').on('click', function (){
        $('.step-one').removeClass('is-active')
        $('.step-two').addClass('is-active')
        $('.progress-box').find('.progress-el span').removeClass('w20').addClass('w40')
        $('.progress-counter p span').text('2')
    })
    $('.step-two .button-box span').on('click', function (){
        $('.step-two').removeClass('is-active')
        $('.step-free').addClass('is-active')
        $('.progress-box').find('.progress-el span').removeClass('w40').addClass('w60')
        $('.progress-counter p span').text('3')
    })
    $('.step-free .button-box span').on('click', function (){
        $('.step-free').removeClass('is-active')
        $('.step-four').addClass('is-active')
        $('.progress-box').find('.progress-el span').removeClass('w60').addClass('w80')
        $('.progress-counter p span').text('4')
    })
    $('.step-four .button-box span').on('click', function (){
        $('.step-four').removeClass('is-active')
        $('.step-five').addClass('is-active')
        $('.progress-box').find('.progress-el span').removeClass('w80').addClass('w100')
        $('.progress-counter p span').text('5')
    })

} );

/*
ON SCROLL PAGE FUNCTIONS
*/

jQuery( window ).on( 'scroll', function() {



} );